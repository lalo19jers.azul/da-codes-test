package com.example.testdacodes.network.data

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.testdacodes.network.remote.ApiClient
import com.example.testdacodes.presentation.models.Drink
import com.example.testdacodes.presentation.models.ListDrinks
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object DrinkRepository {
    fun getMutableLiveData(context: Context, drinkName: String): MutableLiveData<ListDrinks>{

        val mutableLiveData = MutableLiveData<ListDrinks>()

        ApiClient.apiService.getDrinks(strDrink = drinkName).
            enqueue(object : Callback<ListDrinks>{
                override fun onFailure(call: Call<ListDrinks>, t: Throwable) {
                    Log.e("Error", t.localizedMessage)
                }

                override fun onResponse(
                    call: Call<ListDrinks>,
                    response: Response<ListDrinks>
                ) {
                    val drinkResponse = response.body()
                    drinkResponse?.let { mutableLiveData.value = it  }                }

            })

        return mutableLiveData
    }
}