package com.example.testdacodes.network.remote

import androidx.lifecycle.MutableLiveData
import com.example.testdacodes.presentation.models.Drink
import com.example.testdacodes.presentation.models.ListDrinks
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("search.php")
    fun getDrinks(@Query("s") strDrink: String): Call<ListDrinks>
}