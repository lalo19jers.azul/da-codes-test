package com.example.testdacodes.presentation.models

import com.google.gson.annotations.SerializedName

data class ListDrinks(
    @SerializedName("drinks")
    val drinks: ArrayList<Drink>
)

data class Drink(
    @SerializedName("idDrink")
    val id: String? = null,
    @SerializedName("strDrink")
    val strDrink: String? = null,
    @SerializedName("strInstructions")
    val strInstructions: String? = null,
    @SerializedName("strDrinkThumb")
    val strDrinkThumb: String? = null
)