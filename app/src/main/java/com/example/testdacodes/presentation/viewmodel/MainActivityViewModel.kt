package com.example.testdacodes.presentation.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.testdacodes.network.data.DrinkRepository
import com.example.testdacodes.presentation.models.Drink
import com.example.testdacodes.presentation.models.ListDrinks

class MainActivityViewModel(
    private val context: Context,
    private val drinkName: String
): ViewModel() {

    private var listData = MutableLiveData<ListDrinks>()

    init {
        val drinkRepository: DrinkRepository by lazy { DrinkRepository }
        //
        listData = drinkRepository.getMutableLiveData(context = context, drinkName = drinkName)
    }

    fun getData() : MutableLiveData<ListDrinks>{
        return listData
    }


}