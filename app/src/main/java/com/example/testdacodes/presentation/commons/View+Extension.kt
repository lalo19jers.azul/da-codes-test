package com.example.testdacodes.presentation.commons

import android.view.View

/**
 * @param show
 * Show or Hide Elements by boolean show
 * @return [View]
 */
fun View.isVisible(show: Boolean) {
    visibility = if(show) {
        View.VISIBLE
    } else {
        View.GONE
    }
}