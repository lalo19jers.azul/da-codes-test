package com.example.testdacodes.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.testdacodes.databinding.ItemDrinkBinding
import com.example.testdacodes.presentation.models.Drink

class DrinkViewHolder(
    private val binding: ItemDrinkBinding
): RecyclerView.ViewHolder(binding.root) {

    /**
     * @param drink
     * @param onClick
     */
    fun bind(drink: Drink, onClick: (Drink) -> Unit) {
        binding.run {
            ivDrink.load(drink.strDrinkThumb)
            tvDrinkName.text = drink.strDrink
            tvDrinkInstructions.text = drink.strInstructions
            root.setOnClickListener { onClick(drink) }
        }
    }

    /**
     * Inflates item
     */
    companion object {
        fun from(parent: ViewGroup): DrinkViewHolder {
            val layoutInflater = ItemDrinkBinding.inflate(
                LayoutInflater.from(parent.context), parent, false)
            return DrinkViewHolder(layoutInflater)
        }
    }
}