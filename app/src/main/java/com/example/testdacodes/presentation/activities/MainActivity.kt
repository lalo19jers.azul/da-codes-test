package com.example.testdacodes.presentation.activities

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.MenuItemCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testdacodes.R
import com.example.testdacodes.databinding.ActivityMainBinding
import com.example.testdacodes.presentation.adapters.DrinkAdapter
import com.example.testdacodes.presentation.commons.isVisible
import com.example.testdacodes.presentation.models.Drink
import com.example.testdacodes.presentation.models.ListDrinks
import com.example.testdacodes.presentation.viewmodel.MainActivityViewModel
import com.example.testdacodes.presentation.viewmodel.MainActivityViewModelFactory


class MainActivity : AppCompatActivity() {

    /**ViewBinding */
    private val binding: ActivityMainBinding
        by lazy { ActivityMainBinding.inflate(layoutInflater) }
    /** */
    private lateinit var drinkList: MutableList<Drink>
    /** */
    private lateinit var drinkAdapter: DrinkAdapter

    private lateinit var searchView: SearchView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        fetchDrinks("margarita")

    }

    private fun fetchDrinks(drinkName: String) {

        val mainActivityViewModel = ViewModelProviders.of(
            this, MainActivityViewModelFactory(context = this, drinkName = drinkName))
            .get(MainActivityViewModel::class.java)

        mainActivityViewModel.getData().observe(this, object : Observer<ListDrinks>{
            override fun onChanged(newDrinkList: ListDrinks?) {
                drinkList.clear()
                newDrinkList?.let { drinkList.addAll(it.drinks)}
                drinkAdapter.notifyDataSetChanged()
            }
        })

        drinkAdapter = DrinkAdapter(onDrinkRowClick = onDrinkRowClick)
        drinkList = mutableListOf()
        drinkAdapter.submitList(drinkList)
        binding.run {
            rvDrinks.apply {
                layoutManager = LinearLayoutManager(this@MainActivity)
                adapter = drinkAdapter
            }
        }

    }


    /**
     * Handle on RowClick
     */
    private val onDrinkRowClick: (Drink) -> Unit = {
        Log.d("DRINK", it.strDrink!!)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val searchItem: MenuItem = menu!!.findItem(R.id.action_search)

        if (searchItem != null) {
            searchView = MenuItemCompat.getActionView(searchItem) as SearchView
            searchView.setOnCloseListener(object : SearchView.OnCloseListener {
                override fun onClose(): Boolean {
                    return true
                }
            })

            val searchPlate =
                searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
            searchPlate.hint = "Search"
            val searchPlateView: View =
                searchView.findViewById(androidx.appcompat.R.id.search_plate)
            searchPlateView.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    android.R.color.transparent
                )
            )


            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    val searchList = drinkList.filter { it.strDrink == query }
                    if (searchList.isEmpty()) {
                        binding.rvDrinks.isVisible(false)
                        binding.llNotFound.isVisible(true)
                    }
                    else {
                        drinkAdapter.submitList(searchList)
                       drinkAdapter.notifyDataSetChanged()
                    }
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return false
                }
            })

            val searchManager =
                getSystemService(Context.SEARCH_SERVICE) as SearchManager
            searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        }
        return super.onCreateOptionsMenu(menu)
    }



}