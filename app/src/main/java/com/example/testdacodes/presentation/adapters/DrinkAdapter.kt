package com.example.testdacodes.presentation.adapters

import android.view.ViewGroup
import android.widget.Filter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.testdacodes.presentation.models.Drink

class DrinkAdapter(
    private val onDrinkRowClick: (Drink) -> Unit):
    ListAdapter<Drink, DrinkViewHolder>(DrinkDiffUtil()) {

    /**
     * @param parent
     * @param viewType
     * @return [DrinkViewHolder]
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkViewHolder =
        DrinkViewHolder.from(parent)

    /**
     * @param holder
     * @param position
     */
    override fun onBindViewHolder(holder: DrinkViewHolder, position: Int) {
        val drink = getItem(position)
        holder.bind(drink = drink, onClick = onDrinkRowClick)
    }

    internal class DrinkDiffUtil : DiffUtil.ItemCallback<Drink>() {

        /**
         * @param oldItem
         * @param newItem
         * @return [Boolean]
         */
        override fun areItemsTheSame(
            oldItem: Drink,
            newItem: Drink
        ): Boolean = oldItem == newItem

        /**
         * @param oldItem
         * @param newItem
         * @return [Boolean]
         */
        override fun areContentsTheSame(
            oldItem: Drink,
            newItem: Drink
        ): Boolean = oldItem == newItem

    }


}